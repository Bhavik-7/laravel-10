require("dotenv").config();
const cors = require("cors");
const express = require("express");
const app = express();
app.use(cors());
const server = require("http").createServer(app);
const {
	Server
} = require("socket.io");
const io = new Server().listen(server, {
	cors: {
		origin: "*",
		methods: ["GET", "POST"],
		credentials: true
	}
});

const chatBot = process.env.APP_NAME;
const socketPort = 7000;
let chatRoom = "";
let allUsers = [];
let chatRoomUsers = [];
let chatRoomUsersData = [];

io.on("connection", (socket) => {
	console.log(`User connected ${socket.id}`);
	try {
		socket.on("joinRoom", async (data) => {
			const {
				currentUser,
				messages,
				room
			} = data;

			socket.join(room.name);

			if (messages.length) {
				socket.emit("groupMessages", messages);
			}

			let chatTime = new Date();
			io.to(room.name).emit("receiveMessage", {
				socketId: null,
				message: `${currentUser.name} has joined the chat room`,
				user: {
					id: null,
					name: chatBot
				},
				created_at: chatTime,
			});

			socket.emit("receiveMessage", {
				socketId: null,
				message: `Welcome ${currentUser.name}`,
				user: {
					id: null,
					name: chatBot
				},
				created_at: chatTime,
			});

			chatRoom = room.name;
			allUsers.push({
				socketId: socket.id,
				user: currentUser,
				room: room,
			});

			chatRoomUsers = allUsers.filter((user) => user.room.name === chatRoom);
			chatRoomUsersData = {
				currentUser,
				chatRoomUsers
			};

			socket.to(chatRoom).emit("chatRoomUsers", chatRoomUsersData);
			socket.emit("chatRoomUsers", chatRoomUsersData);
		});

		socket.on("sendMessage", async (data) => {
			const {
				message,
				user,
				room,
			} = data;
			io.in(room.name).emit("receiveMessage", {
				socketId: socket.id,
				message: message.message,
				user,
				created_at: message.created_at,
			});
		});

		socket.on("leaveRoom", async () => {
			const user = allUsers.find((user) => user.socketId == socket.id);
			if (user?.user) {
				allUsers = allUsers.filter((user) => user.socketId != socket.id);
				chatRoomUsers = allUsers.filter((user) => user.room.name === chatRoom);
				chatRoomUsersData = {
					currentUser: user,
					chatRoomUsers
				};

				socket.to(user.room.name).emit("removeUserFromDb", user);
				socket.emit("removeUserFromDb", user);
				socket.to(user.room.name).emit("chatRoomUsers", chatRoomUsersData);
				socket.to(user.room.name).emit("receiveMessage", {
					socketId: null,
					message: `${user.user.name} has left the chat`,
					user: {
						id: null,
						name: chatBot
					},
					created_at: new Date(),
				});
				console.log(`${user.user.name} has left the chat`);
				socket.leave(user.room.name);
			}
		});

		socket.on("typing", (data) => {
			socket.broadcast.to(chatRoom).emit("userTyping", data);
		});

		socket.on("disconnect", () => {
			console.log("User disconnected from the chat");
			const user = allUsers.find((user) => user.socketId == socket.id);
			if (user?.user) {
				allUsers = allUsers.filter((user) => user.socketId != socket.id);
				chatRoomUsers = allUsers.filter((user) => user.room.name === chatRoom);
				chatRoomUsersData = {
					currentUser: user,
					chatRoomUsers
				};

				socket.to(chatRoom).emit("removeUserFromDb", user);
				socket.emit("removeUserFromDb", user);
				socket.to(chatRoom).emit("chatRoomUsers", chatRoomUsersData);
				socket.to(chatRoom).emit("receiveMessage", {
					socketId: null,
					message: `${user.user.name} has disconnected from the chat.`,
					user: {
						id: null,
						name: chatBot
					},
					created_at: new Date(),
				});
			}
		});
	} catch (socketException) {
		console.log("Socket exception: " + new Date() + ":", socketException);
	}
});

server.listen(socketPort, () => {
	console.log(`Socket.IO server running at http://localhost:${socketPort}`);
});
