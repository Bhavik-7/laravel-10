$(document).ready(function () {
	let roleTable;
	if ($("#kt_table_roles").length) {
		roleTable = $("#kt_table_roles").DataTable({
			info: true,
			responsive: true,
			processing: false,
			serverSide: true,
			searching: true,
			lengthChange: true,
			ordering: true,
			searching: true,
			ajax: {
				url: roleListUrl,
				data: function (data) {},
				beforeSend: function (request) {
					show_loader();
					request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr("content"));
				},
				complete: function () {
					hide_loader();
				},
			},
			order: [
				[2, "desc"]
			],
			columns: [{
					data: "checkbox",
					name: "checkbox",
					searchable: false,
					orderable: false,
					className: "w-10px px-2",
				},
				{
					data: "name",
					name: "name",
				},
				{
					data: "created_at",
					name: "created_at",
					visible: false
				},
				{
					data: "action",
					name: "action",
					orderable: false,
					searchable: false,
					className: "w-15px px-2",
				},
			],
			drawCallback: function (settings) {
				hide_loader();
				$('td:first-child input[type="checkbox"], th:first-child input[type="checkbox"]').prop("checked", false).trigger("change");
			},
		});

		$(document).on("keyup", '[data-kt-role-table-filter="search"]', delay(function (event) {
			roleTable.search(event.target.value).draw();
		}, 500));
	}

	$(document).on('click', '[data-kt-module-table-select="delete_selected"]', function () {
		let selected_ids = Array.from(document.querySelectorAll('tbody td:first-child input[type="checkbox"]'))
			.filter((checkbox) => checkbox.checked)
			.map((checkbox) => checkbox.value);

		if (selected_ids.length) {
			let delete_element = $(this);
			confirmationBox("Are you sure you want to Delete selected roles?", "You will not be able to recover these roles after deletion.").then(function (response) {
				if (response['isConfirmed']) {
					deleteRoles(delete_element, selected_ids);
				}
			});
		}
	});

	$(document).on('click', '.delete-role', function (event) {
		event.preventDefault();
		let delete_element = $(this);

		confirmationBox("Are you sure you want to Delete this role?", "You will not be able to recover this role after deletion.").then(function (response) {
			if (response['isConfirmed']) {
				deleteRoles(delete_element);
			}
		});
	});

	function deleteRoles(element, selected_ids = []) {
		let method = 'DELETE';
		let url = element.attr('target-url');
		let ajax = ajaxCall(method, url, {
			selected_ids: selected_ids
		});
		show_loader();
		ajax.done(function (response) {
			hide_loader();
			if (response['status']) {
				toastr.success(response['message'], 'Success');
				roleTable.ajax.reload();
			} else {
				toastr.error(response['message'], 'Error');
			}
		});
	}
});
