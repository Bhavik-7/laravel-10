if (user_logged_in == false) {
	var onloadCallback = function () {
		grecaptcha.execute();
	};

	function setResponse(response) {
		document.getElementById('captcha-response').value = response;
	}
}

$(document).ready(function () {
	$(".site-content").attr("style", "background-image:url(" + picture_url + ")");
	$("#file_select").select2({
		tags: true,
		theme: 'classic',
		placeholder: 'Select File or Folder Upload',
		minimumResultsForSearch: -1
	});
});

/** 
 * @Author: BDP 
 * @Date: 2021-04-20
 * @Desc:  validate file input on change event & display file name in DIV.
 */
$(document).on("change", "#file", function (e) {
	e.preventDefault();
	$(this).valid();
	var val;
	var file_names = new Array();
	$.each(this.files, function (k, v) {
		if (k < 5) {
			file_names.push(v.name);
		}
	});
	var f_name = file_names.join(', ');
	if (f_name != null || f_name != '') {
		$(".selected-file-name").show();
		$(".selected-file-name").text(f_name);
	}
});

$.validator.addMethod("notEqualTo", function (value, element, param) {
	return this.optional(element) || value != $(param).val();
}, 'Please enter different email address.');

/** 
 * @Author: BDP 
 * @Date: 2021-04-20
 * @Desc: Validate c-share-form 
 */
$("#c-share-form").validate({
	rules: {
		"file[]": {
			required: true,
		},
		your_email: {
			required: true,
			email: true,
			notEqualTo: "#to_email"
		},
		to_email: {
			required: true,
			email: true,
		},
		transfer_type: {
			required: true,
		},
		termsandconditions: {
			required: true,
		}
	},
	messages: {
		"file[]": {
			required: "Select the File or Folder to upload here.",
		},
		your_email: {
			required: "Enter email.",
			email: "Enter valid email."
		},
		to_email: {
			required: "Enter email.",
			email: "Enter valid email."
		},
		transfer_type: {
			required: "Select transfer type.",
		},
		termsandconditions: {
			required: "Select term & condition box."
		}
	},
	submitHandler: function (form, event) {
		event.preventDefault();
		var is_transfer_email = ($('input[name="transfer_type"]:checked').val() == 'send_transfer_email') ? true : false;
		if (is_transfer_email) {
			var body = $("html, body");
			body.stop().animate({ scrollTop: 0 }, 500, 'swing', function () {
			});

			$('#progressBar').css('display', 'flex');
			$('.progress-bar').css('width', "4%");
			$('.progress-bar span').text("4%");
		}
		// now google invisible re-captcha implemented
		// if (user_logged_in == false) {
		// 	if (grecaptcha.getResponse() == "") {
		// 		toast_alert("Error", "Please verify google reCaptcha!", 'error');
		// 		return;
		// 	}
		// }
		var formData = new FormData(form);
		var get_seldata = $("#c-share-form").find(":input");
		var form_data = get_seldata.serializeArray();
		/*Get input data*/
		$.each(form_data, function (key, input) {
			formData.append(input.name, input.value);
		});
		if ($('input[type="file"]')[0].files.length) {
			if ($('input[type="file"]')[0].files[0].webkitRelativePath != null || $('input[type="file"]')[0].files[0].webkitRelativePath != '') {
				$.each($('input[type="file"]')[0].files, function () {
					formData.append("folders[]", this.webkitRelativePath);
				});
			}
		}
		$.ajax({
			url: BASE_URL + 'quick-share',
			type: 'POST',
			data: formData,
			dataType: 'JSON',
			processData: false,
			contentType: false,
			cache: false,
			beforeSend: function () {
				toggle_loader(true);
			},
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = Math.round((evt.loaded / evt.total) * 100);
						if (percentComplete != 100) {
							$('.progress-bar').css('width', parseInt(percentComplete) + "%");
							$('.progress-bar span').text(parseInt(percentComplete) + "%");
						}
					}
				}, false);
				return xhr;
			},
			success: function (response) {
				$("#c-share-form").trigger("reset");
				if (response.statusCode == 1) {
					// toast_alert("Success", response.message, "success");
					if (response.data != null) {
						$(".ast-container").find("div.SignInUp").html(response.data);
					}
					if(response.data_res != null){
						let resp = response.data_res;
						console.log('call resp+'+resp);
						socket.emit("upload_file_alert", {
							uid: resp.user_id,
							url: resp.url,
							file_name: resp.filename,
						});

						if(resp.touserid != null){
							socket.emit("user_shared_file_alert", {
								uid: resp.touserid,
								url: resp.url,
								file_name: resp.filename,
							});
						}
					}
					if (response.view != null) {
						$(document).find(".share_success_model .modal-body").html(response.view);
						$('#share_success_model').modal('show');
						$('.progress-bar').css('width', "100%");
						$('.progress-bar span').text("100%");
					}
				} else if (response.statusCode == 3) {
					$('.cshare_email_verify_model form .verification_email').val(response.data.from_email).attr("value", response.data.from_email);
					$('.cshare_email_verify_model form .verification_data_from_cshare').val(JSON.stringify(response.data)).attr("value", JSON.stringify(response.data));
					$(".cshare_email_verify_model strong.email_of_verification").text(response.data.from_email).attr("title", response.data.from_email);
					$('#cshare_email_verify_model').modal('show');
					toast_alert("Success", 'Verification code is sent to ' + response.data.from_email + '.', "success");
				} else {
					$('.progress-bar').css('width', "0");
					$('.progress-bar span').text("0");
					$('#progressBar').hide();
					toast_alert("Error", response.message, "error");
				}
			},
		});
	}
});

$(document).on("change", ".select-file-action", function (e) {
	e.preventDefault();
	// console.log($(this).val());
	// var dataSelId = $(this).attr("data-sel-id");
	var dataSelId = $(this).val();
	if (dataSelId == "1") {
		$("#file").attr({
			multiple: "multiple",
			directory: "directory",
			webkitdirectory: "webkitdirectory",
		});
	} else {
		$("#file").removeAttr("multiple directory webkitdirectory");
	}
	$("#file").click();
});

$(document).on("click", ".select-wrapper", function (e) {
	$("#file").click();
});

/** 
 * @Author: BDP 
 * @Date: 2021-04-20
 * @Desc: toggle share type functionality 
 */
$(document).on("click", "input[name=transfer_type]", function (e) {
	if ($("#transfer_type2").is(":checked")) {
		$(".email-to-div").hide();
		$("#to_email").attr("disabled", true);
	} else {
		$(".email-to-div").show();
		$("#to_email").removeAttr("disabled");
	}
});

$(document).on("click", ".cshare_email_verify_model a.email_verification_resend", function (e) {
	let $email = $('.cshare_email_verify_model form .verification_email').val();
	if ($email) {
		$.ajax({
			url: BASE_URL + 'resend-verification-email',
			type: 'POST',
			data: {
				email: $email
			},
			dataType: 'JSON',
			cache: false,
			async: false,
			beforeSend: function () {
				toggle_loader(1);
			},
			success: function (response) {
				if (response.statusCode == 1) {
					toast_alert("Success", response.message, "success");
				} else {
					toast_alert("Error", response.message, "error");
				}
			}
		});
	} else {
		toast_alert("Error", "Something went wrong!", "error");
	}
});

$("#cshare-email-verification").validate({
	rules: {
		verification_code: {
			required: true,
			maxlength: 6,
		}
	},
	messages: {
		verification_code: {
			required: "Enter verification code."
		}
	},
	submitHandler: function (form, event) {
		event.preventDefault();
		let formData = new FormData(form);
		let form_data = $("#cshare-email-verification").find(":input").serializeArray();
		/*Get input data*/
		$.each(form_data, function (key, input) {
			formData.append(input.name, input.value);
		});
		$.ajax({
			url: BASE_URL + 'quick-share-verify-email',
			type: 'POST',
			data: formData,
			dataType: 'JSON',
			processData: false,
			contentType: false,
			cache: false,
			beforeSend: function () {
				toggle_loader(true);
			},
			success: function (response) {
				if (response.statusCode == 1) {
					$('.progress-bar').css('width', "100%");
					$('.progress-bar span').text("100%");
					// toast_alert("Success", response.message, "success");
					$('#cshare_email_verify_model').modal('hide');
					$(".modal-backdrop").remove();
					if (response.data != null) {
						$(".ast-container").find("div.SignInUp").html(response.data);
					}
					if (response.view != null) {
						$(document).find(".share_success_model .modal-body").html(response.view);
						$('#share_success_model').modal('show');
					}
				} else {
					toast_alert("Error", response.message, "error");
				}
			}
		});
	}
});

/* ============================================================== */
// New changes by BDP

function formatState(state) {
	if (!state.id) {
		return state.text;
	}
	var el = $(state.element);
	var available = el.data("available");
	var $state = $("<span>")
		.addClass("option_" + state.id)
		.addClass(!available ? "not-available" : "")
		.append(state.element.text.toLowerCase(), $("<span>"));
	return $state;
}

var prevselection = null;
$("select#file_select")
	.select2({
		minimumResultsForSearch: Infinity,
		templateResult: formatState,
		templateSelection: formatState,
	})
	.on("select2:opening", function (event) {
		prevselection = $(event.target).find(":selected");
		$("select").val(null);
	})
	.on("select2:open", function (event) {
		setTimeout(function () {
			var _optCont = $(
				".select2-results__option .option_" + $(prevselection).val()
			).parent();
			$("li", _optCont.parent()).removeClass(
				"select2-results__option--highlighted"
			);
			$(_optCont).addClass("select2-results__option--highlighted");
		}, 0);
	})
	.on("select2:select", function (event) {
		var _selection = $(event.target).find(":selected");
		var available = _selection.data("available");
		if (available === false) {
			//alert('ok')
			//console.log("ok")
		}
	})
	.on("select2:closing", function (event) {
		if (prevselection != null && $(this).val() == null) {
			$(this).val($(prevselection).val());
		}
	});


function copy_text($input, $value = null, $will_remove = false) {
	if ($input.length) {
		let link = $value;
		if ($value && $will_remove == true) {
			$input.val($value).select();
			document.execCommand("copy");
			$input.remove();

		} else {
			$input.select();
			document.execCommand("copy");

			$input.addClass('animated bounceIn')
				.attr('data-original-title', 'Copied!');
			// .tooltip('open');

			setTimeout(() => {
				$input.removeClass('animated bounceIn')
					.attr('data-original-title', 'Copy Link');
			}, 1000);
		}
		let msg = '<a href="' + link + '" target="_blank"> Open link </a> or use <b>ctrl + v</b> for paste';
		window.toast_alert('Link copied in clipboard', msg, 'success');
	} else {
		window.toast_alert('Error', 'Something went wrong to copy link', 'error');
	}
}

/* Copy Link */
$(document).on('click', '.copy-text', function (event) {
	event.preventDefault();
	let $this = $(this).parent().find('input.link');
	if ($.trim($this.val())) {
		copy_text($this, $this.val());
	}
});

/* ============================================================== */

// START to add the current_page_item / active class to the cshare/quick-share menu of wordpress
setTimeout(() => {
	jQuery('#primary-menu .quick-share-menu').addClass('current_page_item');
}, 100);
// END to add the current_page_item / active class to the cshare/quick-share menu of wordpress