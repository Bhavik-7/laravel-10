const socketPort = 7000;
const protocol = (window.location.protocol === "https:" ? "https://" : "http://");
const socket = io.connect(protocol + window.location.hostname + ':' + socketPort, {
	secure: true,
	rejectUnauthorized: false
});

const messageBox = (message) => {
	if (message.user.id == null) {
		return `
			<div>
				<p class="py-5 px-4 mb-1 rounded-3 bg-secondary">${message.message}</p>
				<p class="small me-3 mb-3 rounded-3 text-muted">${moment(message.created_at).format("Do MMMM YYYY, h:mm:ss a")} | ${message.user.name}</p>
			</div>
		`;
	} else {
		if (message.user.id == atob(loggedInUserId)) {
			return `
			<div class="d-flex flex-row justify-content-end">
				<div>
					<p class="p-5 me-3 mb-1 text-white rounded-3 bg-primary">${message.message}</p>
					<p class="small me-3 mb-3 rounded-3 text-muted">${moment(message.created_at).format("Do MMMM YYYY, h:mm:ss a")} | ${message.user.name}</p>
				</div>
				<div class="symbol symbol-50px">
					<img src="${hostUrl}/media/avatars/300-1.jpg" alt="avatar" class="d-flex align-self-center me-3">
				</div>
			</div>
			`;
		} else {
			return `
			<div class="d-flex flex-row justify-content-start">
				<div class="symbol symbol-50px">
					<img src="${hostUrl}/media/avatars/300-1.jpg" alt="avatar" class="d-flex align-self-center me-3">
				</div>
				<div>
					<p class="p-5 ms-3 mb-1 rounded-3" style="background-color: #f5f6f7;">${message.message}</p>
					<p class="small ms-3 mb-3 rounded-3 text-muted float-end">${moment(message.created_at).format("Do MMMM YYYY, h:mm:ss a")} | ${message.user.name}</p>
				</div>
			</div>
			`;
		}
	}
}

socket.on("groupMessages", async (groupMessages) => {
	groupMessages.map((message) => {
		$(".chatByUser").append(messageBox(message));
	});
});

socket.on("receiveMessage", async (data) => {
	$(".chatByUser").append(messageBox({
		socketId: data.socketId,
		message: data.message,
		user: data.user,
		created_at: data.created_at,
	}));

	setTimeout(() => {
		$(".chatByUser").animate({
			scrollTop: parseInt($(".chatByUser").height()) + 500
		});
	}, 500);
});

socket.on("chatRoomUsers", async (data) => {
	$("ul.usersList").empty();
	data.chatRoomUsers.map((chatRoomUser) => {
		$("ul.usersList").append(`
			<li class="p-2 d-flex mt-2 justify-content-between border-bottom rounded ${chatRoomUser.socketId === socket.id ? 'bg-secondary' : ''}" data-user="${chatRoomUser.user.id}">
				<div class="d-flex flex-row">
					<div class="symbol symbol-45px">
						<img src="${hostUrl}/media/avatars/300-1.jpg" alt="avatar" class="d-flex align-self-center me-3">
					</div>
					<div class="pt-1">
						<p class="fw-bold mb-1">${chatRoomUser.user.name}</p>
						<p class="mb-0 text-active-gray-400 typingBox" style="display:none;">Typing....</p>
					</div>
				</div>
			</li>
		`);
	});
});

socket.on("removeUserFromDb", async (user) => {
	ajaxCall("POST", leaveGroupUrl, {
		group_id: user.room.id,
		user_id: user.user.id
	});
});

socket.on("userTyping", (data) => {
	let uid = atob(data.user);
	
	if (data.typing == true) {
		$(`li[data-user="${uid}"]`).find("p.typingBox").show();
	} else {
		$(`li[data-user="${uid}"]`).find("p.typingBox").hide();
	}
});
