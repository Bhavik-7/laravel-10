"use strict";

var defaultThemeMode = "dark";
var themeMode;
if (document.documentElement) {
	if (document.documentElement.hasAttribute("data-theme-mode")) {
		themeMode = document.documentElement.getAttribute("data-theme-mode");
	} else {
		if (localStorage.getItem("data-theme") !== null) {
			themeMode = localStorage.getItem("data-theme");
		} else {
			themeMode = defaultThemeMode;
		}
	}
	if (themeMode === "system") {
		themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ?
			"dark" :
			"light";
	}
	document.documentElement.setAttribute("data-theme", themeMode);
}

jQuery.fn.extend({
	attrs: function (attributeName) {
		var results = [];
		$.each(this, function (i, item) {
			results.push(item.getAttribute(attributeName));
		});
		return results;
	},
});

function show_loader() {
	$.LoadingOverlay("show", {
		background: "rgba(255, 255, 255, 0.8)",
		image: hostUrl + "/media/loader.svg",
		size: "120",
		maxSize: "120",
		minSize: "120",
	});
}

function hide_loader() {
	$.LoadingOverlay("hide");
}

show_loader();
$(document).ready(function () {
	hide_loader();
});

// Datatable Select all checkbox
$(document).on("change", 'td:first-child input[type="checkbox"], th:first-child input[type="checkbox"]', function () {
	let checkboxes_count = $('tbody td:first-child input[type="checkbox"]:checked').length;
	if (checkboxes_count > 0) {
		$(".table-action-buttons").addClass("d-none");
		$(".delete-all-button-div").removeClass("d-none");
		$('[data-kt-module-table-select="selected_count"]').text(
			checkboxes_count
		);
	} else {
		$(".table-action-buttons").removeClass("d-none");
		$(".delete-all-button-div").addClass("d-none");
	}
});

// Call AJAX function with method_name, route and data
function ajaxCall(method, route, data = null) {
	return $.ajax({
		type: method,
		url: route,
		dataType: "json",
		data: data,
		headers: {
			"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
		},
		error: function (response) {
			ajax_fail(response);
		},
	});
}

// Call AJAX function with method_name, route and data
function formdata_ajaxCall(method, route, data = null) {
	return $.ajax({
		type: method,
		url: route,
		cache: false,
		processData: false,
		contentType: false,
		data: data,
		headers: {
			"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
		},
		error: function (response) {
			ajax_fail(response);
		},
	});
}

// Call AJAX function with method_name, route and data
function form_blob_ajaxCall(method, route, data = null) {
	return $.ajax({
		type: method,
		url: route,
		data: data,
		xhrFields: {
			responseType: "blob",
		},
		headers: {
			"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
		},
		error: function (response) {
			ajax_fail(response);
		},
	});
}

// Handle ajax call failures
function ajax_fail(response) {
	hide_loader();
	if ($(".submit-btn").length) {
		$(".submit-btn").prop("disabled", false);
	}

	let message = response["statusText"];
	let error_type = "Error";

	if (response["responseJSON"]) {
		message = response["responseJSON"]["message"];
	}

	// if (response['status'] == 401) {
	//     sessionStorage.clear();
	//     // unauthorized();
	// }

	if (response["status"] == 422) {
		error_type = "Validation Error";
		if (response["responseJSON"].hasOwnProperty("status")) {
			message = response["responseJSON"]["message"];
		} else {
			$.each(response["responseJSON"]["errors"], function (key, value) {
				message = value[0];
				return false;
			});
		}
	}

	toastr.error(message, error_type);
}

function action_drawer(request = new Object()) {
	let heading = "Action Drawer";
	var drawerEl = document.querySelector("#action_drawer");
	var drawer = KTDrawer.getInstance(drawerEl);

	if (request.hasOwnProperty("heading")) {
		heading = request["heading"];
	}

	$(".action-drawer-header").text(heading);
	drawer.toggle();
	// $('#action_drawer').toggleClass('drawer-on');
}

function confirmationBox(title = "Are you sure you want to delete this record?", text = "You will not be able to recover this record after deletion.", confirmButtonText = "Yes, Delete it!", cancelButtonText = "No, Revert it") {
	return Swal.fire({
		title: title,
		text: text,
		icon: "warning",
		showCancelButton: true,
		buttonsStyling: false,
		confirmButtonText: confirmButtonText,
		cancelButtonText: cancelButtonText,
		reverseButtons: true,
		customClass: {
			confirmButton: "btn fw-bold btn-danger",
			cancelButton: "btn fw-bold btn-active-light-primary",
		},
	});
}

function delay(callback, ms) {
	var timer = 0;
	return function () {
		var context = this,
			args = arguments;
		clearTimeout(timer);
		timer = setTimeout(function () {
			callback.apply(context, args);
		}, ms || 0);
	};
}

function init_select2(element, text = "an option.") {
	element
		.select2({
			placeholder: "Select " + text,
			allowClear: true,
		})
		.on("select2:open", function (e) {
			$(".select2-search__field").attr("placeholder", "Search " + text);
		});
}

function init_select2_options(element, text = "Creting new option, ") {
	let options = element.find("option");
	let option_values = $.map(options, (e) => {
		if ($(e).val()) {
			return $.trim($(e).html());
		}
	});
	element
		.select2({
			tags: true,
			placeholder: "Add new option by typing....",
			allowClear: true,
			matcher: matchStart,
			escapeMarkup: function (markup) {
				return markup;
			},
			templateResult: function (tag) {
				return tag.text;
			},
			templateSelection: function (tag) {
				return tag.text.replace("Creating new " + text, "");
			},
			createTag: function (term) {
				let value = term.term;
				let optionSelected = option_values.some(function (item) {
					return item.includes(value);
				});

				if (optionSelected) {
					return {
						id: value,
						text: value,
					};
				} else {
					return {
						id: value,
						text: "Creating new " +
							text +
							"<strong>" +
							value +
							"</strong>",
					};
				}
			},
		})
		.on("select2:open", function (e) {
			$(".select2-search__field").attr(
				"placeholder",
				"Search/Add option by typing...."
			);
		});
}

function matchStart(params, data) {
	if ($.trim(params.term) === "") {
		return data;
	}
	if (typeof data === "undefined") {
		return null;
	}
	var filteredChildren = [];
	if (data.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
		filteredChildren.push(data);
	}
	if (filteredChildren.length) {
		var modifiedData = $.extend({}, data, true);
		modifiedData.children = filteredChildren;
		return modifiedData;
	}
	return null;
}

// Generate random integer number in given range
function getRandomInt(min = 999, max = 9999999) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Image image not exist then load defaul image
// function image_error() {
//     var image_tags = $('body').find('section').find('img');
//     var image_tags_length = image_tags.length;
//     for (var i = 0; i < image_tags_length; i++) {
//         $(image_tags[i]).on('error', function() {
//             $(this).attr("onerror", null);
//             $(this).attr("src", default_image);
//         });
//     }
// }

// function lazyload_image() {
//     setTimeout(function () {
//         if (typeof $('body').find('.lazy').lazy !== "undefined") {
//             // Load Image Lazy
//             $('body').find('.lazy').lazy({
//                 // your configuration goes here
//                 chainable: false,
//                 scrollDirection: 'vertical',
//                 effect: 'fadeIn',
//                 visibleOnly: true,
//                 combined: true,
//                 delay: 2000,
//                 effectTime: 2000,
//                 threshold: 0,
//                 throttle: 1000,
//                 // placeholder: "data:image/gif;base64,R0lGODlhEALAPQAPzl5uLr9Nrl8e7...",
//                 onError: function(element) {
//                     element.attr('src', default_image);
//                 }
//             });
//         }
//     }, 200);
// }
