<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
 */

Route::get('/', function () {
	return view('welcome');
})->name('home');

// Social Login Routes
Route::get('login/{provider}', [HomeController::class, 'socialLogin'])->name('social-login');
Route::get('login/{provider}/callback', [HomeController::class, 'socialLoginCallback']);
// Social Login Routes

Route::middleware(['auth', 'verified'])->group(function () {
	Route::get('/dashboard', [HomeController::class, 'user_dashboard'])->name('dashboard');
	Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
	Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
	Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

	Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['role:admin']], function () {
		Route::get('/', [HomeController::class, 'index'])->name('dashboard');
		Route::resource('roles', RoleController::class, ['names' => 'role']);
	});
});
require __DIR__.'/auth.php';
