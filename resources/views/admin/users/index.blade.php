@extends('admin.layouts.app')

@section('title', 'Users')

@section('css')
@endsection

@section('content')
	<!--begin::Toolbar-->
	<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
		<!--begin::Toolbar container-->
		<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
			<!--begin::Page title-->
			<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
					<!--begin::Item-->
					<li class="breadcrumb-item text-primary">
						<a href="{{ route('admin.dashboard') }}" class="text-primary text-hover-primary">Dashboard</a>
					</li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item">
						<span class="bullet bg-gray-400 w-5px h-2px"></span>
					</li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item text-muted">Users</li>
					<!--end::Item-->
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page title-->
		</div>
		<!--end::Toolbar container-->
	</div>
	<!--end::Toolbar-->

	<!--begin::Content-->
	<div id="kt_app_content" class="app-content flex-column-fluid">
		<!--begin::Content container-->
		<div id="kt_app_content_container" class="app-container container-xxl">
			<!--begin::Card-->
			<div class="card">
				<!--begin::Card header-->
				<div class="card-header border-0 pt-6">
					<!--begin::Card title-->
					<div class="card-title">
						<!--begin::Search-->
						<div class="d-flex align-items-center position-relative my-1">
							<!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
							<span class="svg-icon svg-icon-1 position-absolute ms-6">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
									<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1"
										transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
									<path
										d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
										fill="currentColor" />
								</svg>
							</span>
							<!--end::Svg Icon-->
							<input type="text" data-kt-user-table-filter="search" class="form-control form-control-solid w-250px ps-14"
								placeholder="Search user" />
						</div>
						<!--end::Search-->
					</div>
					<!--begin::Card title-->
					<!--begin::Card toolbar-->
					<div class="card-toolbar">
						<!--begin::Toolbar-->
						<div class="d-flex justify-content-end" data-kt-module-table-toolbar="base">
							<!--begin::Add user-->
							<a href="{{ route('admin.user.create') }}" class="btn btn-light-primary">
								<!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
								<span class="svg-icon svg-icon-3">
									<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
										<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5"
											fill="currentColor"></rect>
										<rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
											transform="rotate(-90 10.8891 17.8033)" fill="currentColor"></rect>
										<rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="currentColor"></rect>
									</svg>
								</span>
								<!--end::Svg Icon-->Add User
							</a>
							<!--end::Add user-->
						</div>
						<!--end::Toolbar-->

						<!--begin::Group actions-->
						<div class="d-flex justify-content-end align-items-center d-none delete-all-button-div ms-3"
							data-kt-module-table-toolbar="selected">
							<div class="fw-bolder me-5">
								<span class="me-2" data-kt-module-table-select="selected_count"></span>Selected
							</div>
							<button type="button" class="btn btn-danger" data-kt-module-table-select="delete_selected"
								target-url="{{ route('admin.user.destroy', ['user' => 0]) }}">Delete Selected</button>
						</div>
						<!--end::Group actions-->
					</div>
					<!--end::Card toolbar-->
				</div>
				<!--end::Card header-->
				<!--begin::Card body-->
				<div class="card-body py-4">
					<!--begin::Table-->
					<table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
						<!--begin::Table head-->
						<thead>
							<!--begin::Table row-->
							<tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
								<th class="w-10px px-2">
									<div class="form-check form-check-sm form-check-custom form-check-solid me-3">
										<input class="form-check-input" type="checkbox" data-kt-check="true"
											data-kt-check-target="#kt_table_users form-check-input" value="1" />
									</div>
								</th>
								<th class="min-w-150px">User</th>
								<th>last name</th>
								<th class="min-w-150px">Email</th>
								<th class="w-20px">Status</th>
								<th>Create Date</th>
								<th class="text-center min-w-100px">Actions</th>
							</tr>
							<!--end::Table row-->
						</thead>
						<!--end::Table head-->
						<!--begin::Table body-->
						<tbody class="text-gray-600 fw-semibold"></tbody>
						<!--end::Table body-->
					</table>
					<!--end::Table-->
				</div>
				<!--end::Card body-->
			</div>
			<!--end::Card-->
		</div>
		<!--end::Content container-->
	</div>
	<!--end::Content-->
@endsection

@section('js')
	<script type="text/javascript">
		var userListUrl = "{{ route('admin.user.index') }}";
	</script>
	<script src="{{ asset('assets/js/admin/user.js') }}"></script>
@endsection
