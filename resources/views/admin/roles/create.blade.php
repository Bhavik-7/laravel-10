@extends('admin.layouts.app')

@section('title', isset($category) ? 'Edit Category' : 'Add Category')

@section('css')
@endsection

@section('content')
	<!--begin::Toolbar-->
	<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
		<!--begin::Toolbar container-->
		<div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
			<!--begin::Page title-->
			<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
				<!--begin::Breadcrumb-->
				<ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
					<!--begin::Item-->
					<li class="breadcrumb-item text-primary">
						<a href="{{ route('admin.dashboard') }}" class="text-primary text-hover-primary">Dashboard</a>
					</li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item">
						<span class="bullet bg-gray-400 w-5px h-2px"></span>
					</li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item text-primary">
						<a href="{{ route('admin.category.index') }}" class="text-primary text-hover-primary">Categories</a>
					</li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item">
						<span class="bullet bg-gray-400 w-5px h-2px"></span>
					</li>
					<!--end::Item-->
					<!--begin::Item-->
					<li class="breadcrumb-item text-muted">{{ isset($category) ? 'Edit ' : 'Create ' }}</li>
					<!--end::Item-->
				</ul>
				<!--end::Breadcrumb-->
			</div>
			<!--end::Page title-->
		</div>
		<!--end::Toolbar container-->
	</div>
	<!--end::Toolbar-->

	<!--begin::Content-->
	<div id="kt_app_content" class="app-content flex-column-fluid">
		<!--begin::Content container-->
		<div id="kt_app_content_container" class="app-container container-xxl">
			<div class="row">
				<div class="col-md-8 offset-md-2">
					<!--begin::Card-->
					<div class="card">
						<!--begin::Card header-->
						<div class="card-header" id="kt_chat_contacts_header">
							<!--begin::Card title-->
							<div class="card-title">
								<h2>{{ isset($category) ? 'Edit ' : 'Add ' }} Category</h2>
							</div>
							<!--end::Card title-->
						</div>
						<!--end::Card header-->
						<!--begin::Card body-->
						<div class="card-body py-4">
							<!--begin::Form-->
							<form id="category_form" class="form" novalidate="novalidate" method="POST"
								action="{{ isset($category) ? route('admin.category.update', $category) : route('admin.category.store') }}"
								enctype="multipart/form-data">
								@csrf
								@isset($category)
									<input type="hidden" name="_method" value="put" />
								@endisset
								<div class="row row-cols-1 row-cols-sm-2 rol-cols-md-1 row-cols-lg-2">
									<!--begin::Col-->
									<div class="col">
										<!--begin::Input group-->
										<div class="fv-row mb-7 fv-plugins-icon-container">
											<!--begin::Label-->
											<label class="required fs-6 fw-semibold form-label mt-3">Name</label>
											<!--end::Label-->
											<!--begin::Input-->
											<input type="name" class="form-control form-control-solid @error('name') is-invalid @enderror"
												name="name" value="{{ old('name', @$category->name) }}" placeholder="Name">
											<!--end::Input-->
											@error('name')
												<div id="name-error" class="invalid-feedback">{{ $message }}</div>
											@enderror
											<div class="fv-plugins-message-container invalid-feedback"></div>
										</div>
										<!--end::Input group-->
									</div>
									<!--end::Col-->
									<!--begin::Col-->
									<div class="col">
										<!--begin::Input group-->
										<div class="fv-row mb-7">
											<!--begin::Label-->
											<label class="fs-6 fw-semibold form-label mt-3">
												Parent Category
											</label>
											<!--end::Label-->
											<!--begin::Input-->
											<div class="input-group input-group-solid flex-nowrap">
												<span class="input-group-text"><i class="bi bi-calendar fs-4"></i></span>
												<div class="overflow-hidden flex-grow-1">
													<select class="form-select form-select-solid rounded-start-0 border-start" data-control="select2"
														data-placeholder="Select an option" name="parent_category_id">
														<option></option>
														@forelse ($categories as $parent_category)
															<option value="{{ $parent_category->id }}"
																{{ old('parent_category_id', @$category->parent_category_id) == $parent_category->id ? 'selected' : '' }}>
																{{ ucwords($parent_category->name) }}</option>
														@empty
														@endforelse
													</select>
												</div>
											</div>
											<!--end::Input-->
										</div>
										<!--end::Input group-->
									</div>
									<!--end::Col-->
								</div>
								<div class="row">
									<!--begin::Col-->
									<div class="col-md-12">
										<div class="row mb-6">
											<!--begin::Label-->
											<label class="col-md-3 col-form-label fw-bold fs-6">Category logo</label>
											<!--end::Label-->
											<!--begin::Col-->
											<div class="col-md-9">
												<!--begin::Image input-->
												<div class="image-input image-input-outline" data-kt-image-input="true"
													style="background-image: url({{ asset('assets/media/default.jpg') }})">
													<!--begin::Preview existing avatar-->
													<div class="image-input-wrapper w-125px h-125px"
														style="background-image: url( {{ @$category->full_image_url ?? asset('assets/media/default.jpg') }} );">
													</div>
													<!--end::Preview existing avatar-->
													<!--begin::Label-->
													<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
														data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change">
														<i class="bi bi-pencil-fill fs-7"></i>
														<!--begin::Inputs-->
														<input type="file" name="image" accept="image/*" />
														<!--end::Inputs-->
													</label>
													<!--end::Label-->
													<!--begin::Cancel-->
													<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
														data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel">
														<i class="bi bi-x fs-2"></i>
													</span>
													<!--end::Cancel-->
													<!--begin::Remove-->
													<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
														data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove">
														<i class="bi bi-x fs-2"></i>
													</span>
													<!--end::Remove-->
												</div>
												<!--end::Image input-->
												<!--begin::Hint-->
												<div class="form-text">Allowed file types: Images/*.</div>
												<!--end::Hint-->
											</div>
											<!--end::Col-->
										</div>
									</div>
									<!--end::Col-->
								</div>
								<!--begin::Actions-->
								<div class="text-center">
									<button type="submit" class="btn btn-primary submit-btn" data-kt-services-modal-action="submit">
										<span class="indicator-label">Submit</span>
										<span class="indicator-progress">Please wait...
											<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
									</button>
									<a href="javascript:history.back()" class="btn btn-danger me-3">Back</a>
								</div>
								<!--end::Actions-->
							</form>
							<!--end::Form-->
						</div>
						<!--end::Card body-->
					</div>
					<!--end::Card-->
				</div>
			</div>
		</div>
		<!--end::Content container-->
	</div>
	<!--end::Content-->
@endsection

@section('js')
	@if (isset($category))
		{!! JsValidator::formRequest('App\Http\Requests\Category\UpdateCategoryRequest', '#category_form') !!}
	@else
		{!! JsValidator::formRequest('App\Http\Requests\Category\StoreCategoryRequest', '#category_form') !!}
	@endif

	<script type="text/javascript">
		var categoryListUrl = "{{ route('admin.category.index') }}";
	</script>
	<script src="{{ asset('assets/js/admin/category.js') }}"></script>
@endsection
