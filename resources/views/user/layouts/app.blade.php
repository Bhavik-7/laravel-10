<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
	<base href="" />
	<title>@yield('title') {{ ' | ' . env('APP_NAME') }}</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="shortcut icon" href="{{ asset('assets/media/laravel.svg') }}" />
	<!--begin::Fonts(mandatory for all pages)-->
	{{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=figtree:300" /> --}}
	<!-- Fonts -->
	<link rel="preconnect" href="https://fonts.bunny.net">
	<link href="https://fonts.bunny.net/css?family=figtree:300,400,500,600&display=swap" rel="stylesheet" />
	<!--end::Fonts-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
        integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!--begin::Vendor Stylesheets(used for this page only)-->
	<link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
	<!--end::Vendor Stylesheets-->
	<!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
	<link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
	<!--end::Global Stylesheets Bundle-->
	@yield('css')
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" data-bs-spy="scroll" data-bs-target="#kt_landing_menu" data-bs-offset="200"
	class="position-relative">
	<!--begin::Main-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Header Section-->
		<div class="mb-0" id="home">
			<!--begin::Wrapper-->
			<div class="landing-dark-bg">
				<!--begin::Header-->
				<div class="landing-header" data-kt-sticky="true" data-kt-sticky-name="landing-header"
					data-kt-sticky-offset="{default: '200px', lg: '300px'}">
					<!--begin::Container-->
					<div class="container">
						<!--begin::Wrapper-->
						<div class="d-flex align-items-center justify-content-between">
							<!--begin::Logo-->
							<div class="d-flex align-items-center flex-equal">
								<!--begin::Mobile menu toggle-->
								<button class="btn btn-icon btn-active-color-primary me-3 d-flex d-lg-none" id="kt_landing_menu_toggle">
									<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
									<span class="svg-icon svg-icon-2hx">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z"
												fill="black" />
											<path opacity="0.3"
												d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z"
												fill="black" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</button>
								<!--end::Mobile menu toggle-->
								<!--begin::Logo image-->
								<a href="{{ route('home') }}" class="d-flex align-items-center">
									<img alt="Logo" src="{{ asset('assets/media/laravel.svg') }}"
										class="logo-default h-25px h-lg-30px" />
									<img alt="Logo" src="{{ asset('assets/media/laravel.svg') }}"
										class="logo-sticky h-30px h-lg-30px" />
									<p class="m-0 ms-2">{{ env('APP_NAME') }}</p>
								</a>
								<!--end::Logo image-->
							</div>
							<!--end::Logo-->
							<!--begin::Menu wrapper-->
							<div class="d-lg-block" id="kt_header_nav_wrapper">
								<div class="d-lg-block p-5 p-lg-0" data-kt-drawer="true" data-kt-drawer-name="landing-menu"
									data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="200px"
									data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_landing_menu_toggle" data-kt-swapper="true"
									data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav_wrapper'}">
									<!--begin::Menu-->
									<div
										class="menu menu-column flex-nowrap menu-rounded menu-lg-row menu-title-gray-500 menu-state-title-primary nav nav-flush fs-5 fw-bold"
										id="kt_landing_menu">
										<!--begin::Menu item-->
										<div class="menu-item">
											<!--begin::Menu link-->
											<a class="menu-link nav-link @if (request()->routeIs('home')) active @endif py-3 px-4 px-xxl-6"
												href="{{ route('home') }}">Home</a>
											<!--end::Menu link-->
										</div>
										<!--end::Menu item-->
										<!--begin::Menu item-->
										<div class="menu-item">
											<!--begin::Menu link-->
											{{-- <a class="menu-link nav-link @if (request()->routeIs('plans')) active @endif py-3 px-4 px-xxl-6"
												href="{{ route('plans') }}">Plans</a> --}}
											<!--end::Menu link-->
										</div>
										<!--end::Menu item-->
										<!--begin::Menu item-->
										<div class="menu-item">
											<!--begin::Menu link-->
											{{-- <a class="menu-link nav-link @if (request()->routeIs('share')) active @endif py-3 px-4 px-xxl-6"
												href="{{ route('share') }}">Share</a> --}}
											<!--end::Menu link-->
										</div>
										<!--end::Menu item-->
									</div>
									<!--end::Menu-->
								</div>
							</div>
							<!--end::Menu wrapper-->
							<!--begin::Toolbar-->
							<div class="flex-equal text-end ms-1">
								@if (Auth::check())
									<a href="{{ route('dashboard') }}" class="btn btn-sm btn-light">Dashboard</a>
									<a href="{{ route('logout') }}"
										onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
										class="btn btn-sm btn-secondary">Logout</a>
								@else
									<a href="{{ route('login') }}" class="btn btn-sm btn-success">Sign In</a>
									<a href="{{ route('register') }}" class="btn btn-sm btn-primary">Sign Up</a>
								@endif
							</div>
							<!--end::Toolbar-->
						</div>
						<!--end::Wrapper-->
					</div>
					<!--end::Container-->
				</div>
				<!--end::Header-->
			</div>
			<!--end::Wrapper-->
		</div>
		<!--end::Header Section-->
		@yield('content')
		<!--begin::Footer Section-->
		<div class="mb-0">
			<!--begin::Wrapper-->
			<div class="landing-footer-bg">
				<!--begin::Container-->
				<div class="container">
					<!--begin::Wrapper-->
					<div class="d-flex flex-column flex-md-row flex-stack py-2">
						<!--begin::Copyright-->
						<div class="d-flex align-items-center order-2 order-md-1">
							<!--begin::Logo-->
							<a href="{{ route('home') }}" class="d-flex align-items-center">
								<img alt="Logo" src="{{ asset('assets/media/laravel.svg') }}" class="h-30px" />
								<p class="m-0 ms-2">{{ env('APP_NAME') }}</p>
							</a>
							<!--end::Logo image-->
							<!--begin::Logo image-->
							<span class="mx-5 fs-6 fw-bold text-gray-600 pt-1">&copy; {{ date('Y') }}
								{{ env('APP_NAME') }}.</span>
							<!--end::Logo image-->
						</div>
						<!--end::Copyright-->
						<!--begin::Menu-->
						<ul class="menu menu-gray-600 menu-hover-primary fw-bold fs-6 fs-md-5 order-1 mb-5 mb-md-0">
							<li class="menu-item">
								<a href="about" class="menu-link px-2">About</a>
							</li>
							<li class="menu-item mx-5">
								<a href="support" class="menu-link px-2">Support</a>
							</li>
							<li class="menu-item mx-5">
								<a class="menu-link px-2">Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP
									v{{ PHP_VERSION }})</a>
							</li>
						</ul>
						<!--end::Menu-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Container-->
			</div>
			<!--end::Wrapper-->
		</div>
		<!--end::Footer Section-->
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1"
						transform="rotate(90 13 6)" fill="black" />
					<path
						d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
						fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
	</div>
	<!--end::Main-->

	<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
		@csrf
	</form>
	<!--begin::Javascript-->
	<script>
		var hostUrl = "{{ asset('assets/') }}";
		var loggedInUserId = btoa("{{ auth()->check() ? auth()->user()->id : null }}");
	</script>
	<!--begin::Global Javascript Bundle(mandatory for all pages)-->
	<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
	<script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/loadingoverlay.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/socket.io.js') }}"></script>
	<!--end::Global Javascript Bundle-->
	<!--begin::Vendors Javascript(used for this page only)-->
	<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
	<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
	<!--end::Vendors Javascript-->
	<!--begin::Custom Javascript(used for this page only)-->
	<script src="{{ asset('assets/js/common.js') }}"></script>
	<script src="{{ asset('assets/js/socket-script.js') }}"></script>
	<!--end::Custom Javascript-->
	<!--Laravel JS validation -->
	<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"></script>
	<!--Laravel JS validation -->
	<!--end::Javascript-->

	<script type="text/javascript">
		/* START: toast messages */
		toastr.options = {
			"closeButton": true,
			"progressBar": true
		}

		@foreach (['error', 'warning', 'success', 'info'] as $message)
			@if (Session::has($message))
				toastr.{{ $message }}("{{ Session::get($message) }}", "{{ ucfirst($message) }}");
				{{ Session::forget($message) }}
			@endif
		@endforeach
		/* END: toast messages */
	</script>
	@yield('js')
</body>
<!--end::Body-->

</html>
