{{-- <x-guest-layout>
	<div class="mb-4 text-sm text-gray-600 dark:text-gray-400">
		{{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
	</div>

	<!-- Session Status -->
	<x-auth-session-status class="mb-4" :status="session('status')" />

	<form method="POST" action="{{ route('password.email') }}">
		@csrf

		<!-- Email Address -->
		<div>
			<x-input-label for="email" :value="__('Email')" />
			<x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required
				autofocus />
			<x-input-error :messages="$errors->get('email')" class="mt-2" />
		</div>

		<div class="flex items-center justify-end mt-4">
			<x-primary-button>
				{{ __('Email Password Reset Link') }}
			</x-primary-button>
		</div>
	</form>
</x-guest-layout> --}}
@extends('auth.app')

@section('title', 'Forgot Password')

@section('css')
	<!--begin::Page bg image-->
	<style>
		body {
			background-image: url({{ asset('assets/media/auth/bg10.jpeg') }});
		}

		[data-theme="dark"] body {
			background-image: url({{ asset('assets/media/auth/bg10-dark.jpeg') }});
		}
	</style>
	<!--end::Page bg image-->
@endsection

@section('content')
	<!--begin::Root-->
	<div class="d-flex flex-column flex-root" id="kt_app_root">
		<!--begin::Authentication - Sign-in -->
		<div class="d-flex flex-column flex-lg-row flex-column-fluid">
			<!--begin::Aside-->
			<div class="d-flex flex-lg-row-fluid">
				<!--begin::Content-->
				<div class="d-flex flex-column flex-center pb-0 pb-lg-5 p-5 w-100">
					<a href="{{ route('home') }}">
					<!--begin::Image-->
					<img class="theme-light-show mx-auto mw-100 w-150px w-lg-300px mb-10 mb-lg-20"
						src="{{ asset('assets/media/auth/agency.png') }}" alt="" />
					<img class="theme-dark-show mx-auto mw-100 w-150px w-lg-300px mb-10 mb-lg-20"
						src="{{ asset('assets/media/auth/agency-dark.png') }}" alt="" />
					<!--end::Image-->
					</a>
					<!--begin::Title-->
					<h1 class="text-gray-800 fs-2qx fw-bold text-center mb-7">Fast, Efficient and Productive</h1>
					<!--end::Title-->
				</div>
				<!--end::Content-->
			</div>
			<!--begin::Aside-->
			<!--begin::Body-->
			<div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-8">
				<!--begin::Wrapper-->
				<div class="bg-body d-flex flex-center rounded-4 w-md-600px p-10">
					<!--begin::Content-->
					<div class="w-md-400px">
						<!--begin::Form-->
						<form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" method="POST" action="{{ route('password.email') }}">
							@csrf
                            <!--begin::Heading-->
							<div class="text-center mb-10">
								<!--begin::Title-->
								<p class="text-dark mb-3">{{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}</p>
								<!--end::Title-->
							</div>
							<!--begin::Heading-->

                            <!--begin::Input group=-->
							<div class="fv-row mb-4">
								<!--begin::Label-->
								<label class="form-label fs-6 text-gray-500">Email</label>
								<!--end::Label-->
								<!--begin::Email-->
								<input class="form-control bg-transparent" type="text" value="{{ old('email') }}" name="email"
									autocomplete="off" placeholder="Email" />
								<!--end::Input-->
								@error('email')
									<label for="email" class="text-danger mt-2">{{ $message }}</label>
								@enderror
								<!--end::Email-->
							</div>

                            <!--begin::Submit button-->
							<div class="d-grid mb-10">
								<button type="submit" id="kt_sign_in_submit" class="btn btn-primary">
									<!--begin::Indicator label-->
									<span class="indicator-label">{{ __('Email Password Reset Link') }}</span>
									<!--end::Indicator label-->
									<!--begin::Indicator progress-->
									<span class="indicator-progress">Please wait...
										<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
									<!--end::Indicator progress-->
								</button>
							</div>
							<!--end::Submit button-->
						</form>
						<!--end::Form-->
					</div>
					<!--end::Content-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Body-->
		</div>
		<!--end::Authentication - Sign-in-->
	</div>
	<!--end::Root-->
@endsection

@section('js')
@endsection
