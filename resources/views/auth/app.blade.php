<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
	<base href="" />
	<title>@yield('title') {{ ' | ' . env('APP_NAME') }}</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<link rel="shortcut icon" href="{{ asset('assets/media/laravel.svg') }}" />
	<!--begin::Fonts(mandatory for all pages)-->
	{{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=figtree:300" /> --}}
	<!-- Fonts -->
	<link rel="preconnect" href="https://fonts.bunny.net">
	<link href="https://fonts.bunny.net/css?family=figtree:300,400,500,600&display=swap" rel="stylesheet" />
	<!--end::Fonts-->
	<!--begin::Vendor Stylesheets(used for this page only)-->
	<link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
	<!--end::Vendor Stylesheets-->
	<!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
	<link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
	<!--end::Global Stylesheets Bundle-->
	@yield('css')
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" data-bs-spy="scroll" data-bs-target="#kt_landing_menu" data-bs-offset="200"
	class="position-relative">
	@yield('content')

	<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
		@csrf
	</form>
	<!--begin::Javascript-->
	<script>
		var hostUrl = "{{ asset('assets/') }}";
		var loggedInUserId = btoa("{{ auth()->check() ? auth()->user()->id : null }}");
	</script>
	<!--begin::Global Javascript Bundle(mandatory for all pages)-->
	<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
	<script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/loadingoverlay.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/socket.io.js') }}"></script>
	<!--end::Global Javascript Bundle-->
	<!--begin::Vendors Javascript(used for this page only)-->
	<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
	<script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
	<!--end::Vendors Javascript-->
	<!--begin::Custom Javascript(used for this page only)-->
	<script src="{{ asset('assets/js/common.js') }}"></script>
	<script src="{{ asset('assets/js/socket-script.js') }}"></script>
	<!--end::Custom Javascript-->
	<!--Laravel JS validation -->
	<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"></script>
	<!--Laravel JS validation -->
	<!--end::Javascript-->

	<script type="text/javascript">
		/* START: toast messages */
		toastr.options = {
			"closeButton": true,
			"progressBar": true
		}

		@foreach (['error', 'warning', 'success', 'info'] as $message)
			@if (Session::has($message))
				toastr.{{ $message }}("{{ Session::get($message) }}", "{{ ucfirst($message) }}");
				{{ Session::forget($message) }}
			@endif
		@endforeach
		/* END: toast messages */
	</script>
	@yield('js')
</body>
<!--end::Body-->

</html>
