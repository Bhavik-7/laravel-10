{{-- <x-guest-layout>
	<form method="POST" action="">
		@csrf

		<!-- First Name -->
		<div>
			<x-input-label for="first_name" :value="__('First Name')" />
			<x-text-input id="first_name" class="block mt-1 w-full" type="text" name="first_name" :value="old('first_name')" required
				autofocus />
			<x-input-error :messages="$errors->get('first_name')" class="mt-2" />
		</div>

		<!-- Last Name -->
		<div class="mt-4">
			<x-input-label for="last_name" :value="__('Last Name')" />
			<x-text-input id="last_name" class="block mt-1 w-full" type="text" name="last_name" :value="old('last_name')" required
				autofocus />
			<x-input-error :messages="$errors->get('last_name')" class="mt-2" />
		</div>

		<!-- Email Address -->
		<div class="mt-4">
			<x-input-label for="email" :value="__('Email')" />
			<x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
			<x-input-error :messages="$errors->get('email')" class="mt-2" />
		</div>

		<!-- Password -->
		<div class="mt-4">
			<x-input-label for="password" :value="__('Password')" />

			<x-text-input id="password" class="block mt-1 w-full" type="password" name="password" required
				autocomplete="new-password" />

			<x-input-error :messages="$errors->get('password')" class="mt-2" />
		</div>

		<!-- Confirm Password -->
		<div class="mt-4">
			<x-input-label for="password_confirmation" :value="__('Confirm Password')" />

			<x-text-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation"
				required />

			<x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
		</div>

		<div class="flex items-center justify-end mt-4">
			<a
				class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800"
				href="{{ route('login') }}">
				{{ __('Already registered?') }}
			</a>

			<x-primary-button class="ml-4">
				{{ __('Register') }}
			</x-primary-button>
		</div>
	</form>
</x-guest-layout> --}}


@extends('auth.app')

@section('title', 'Register')

@section('css')
	<!--begin::Page bg image-->
	<style>
		body {
			background-image: url({{ asset('assets/media/auth/bg10.jpeg') }});
		}

		[data-theme="dark"] body {
			background-image: url({{ asset('assets/media/auth/bg10-dark.jpeg') }});
		}
	</style>
	<!--end::Page bg image-->
@endsection

@section('content')
	<!--begin::Root-->
	<div class="d-flex flex-column flex-root" id="kt_app_root">
		<!--begin::Authentication - Sign-in -->
		<div class="d-flex flex-column flex-lg-row flex-column-fluid">
			<!--begin::Aside-->
			<div class="d-flex flex-lg-row-fluid">
				<!--begin::Content-->
				<div class="d-flex flex-column flex-center pb-0 pb-lg-5 p-5 w-100">
					<a href="{{ route('home') }}">
					<!--begin::Image-->
					<img class="theme-light-show mx-auto mw-100 w-150px w-lg-300px mb-10 mb-lg-20"
						src="{{ asset('assets/media/auth/agency.png') }}" alt="" />
					<img class="theme-dark-show mx-auto mw-100 w-150px w-lg-300px mb-10 mb-lg-20"
						src="{{ asset('assets/media/auth/agency-dark.png') }}" alt="" />
					<!--end::Image-->
					</a>
					<!--begin::Title-->
					<h1 class="text-gray-800 fs-2qx fw-bold text-center mb-7">Fast, Efficient and Productive</h1>
					<!--end::Title-->
				</div>
				<!--end::Content-->
			</div>
			<!--begin::Aside-->
			<!--begin::Body-->
			<div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-8">
				<!--begin::Wrapper-->
				<div class="bg-body d-flex flex-center rounded-4 w-md-600px p-10">
					<!--begin::Content-->
					<div class="w-md-400px">
						<!--begin::Form-->
						<form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" method="POST" action="{{ route('register') }}">
							@csrf
							<!--begin::Heading-->
							<div class="text-center mb-10">
								<!--begin::Title-->
								<h1 class="text-dark mb-3">Sign Up with {{ env('APP_NAME') }}</h1>
								<!--end::Title-->
							</div>
							<!--begin::Heading-->
							<!--begin::Login options-->
							<div class="row g-3 mb-9">
								<!--begin::Col-->
								<div class="col-md-6">
									<!--begin::Google link=-->
									<a href="{{ route('social-login', 'google') }}"
										class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
										<img alt="Logo" src="{{ asset('assets/media/svg/brand-logos/google-icon.svg') }}"
											class="h-15px me-3" />Sign in with Google</a>
									<!--end::Google link=-->
								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6">
									<!--begin::Google link=-->
									<a href="{{ route('social-login', 'facebook') }}"
										class="btn btn-flex btn-outline btn-text-gray-700 btn-active-color-primary bg-state-light flex-center text-nowrap w-100">
										<img alt="Logo" src="{{ asset('assets/media/svg/brand-logos/facebook-4.svg') }}"
											class="theme-light-show h-15px me-3" />
										<img alt="Logo" src="{{ asset('assets/media/svg/brand-logos/facebook-4.svg') }}"
											class="theme-dark-show h-15px me-3" />Sign in with Facebook</a>
									<!--end::Google link=-->
								</div>
								<!--end::Col-->
							</div>
							<!--end::Login options-->
							<!--begin::Separator-->
							<div class="separator separator-content my-14">
								<span class="w-125px text-gray-500 fw-semibold fs-7">Or with email</span>
							</div>
							<!--end::Separator-->
							<!--begin::Input group=-->
							<div class="fv-row mb-3">
								<!--begin::Label-->
								<label class="form-label fs-6 text-gray-500">First Name</label>
								<!--end::Label-->
								<!--begin::First Name-->
								<input class="form-control bg-transparent" type="text" value="{{ old('first_name') }}" name="first_name"
									autocomplete="off" placeholder="First Name" />
								<!--end::Input-->
								@error('first_name')
									<label for="first_name" class="text-danger mt-2">{{ $message }}</label>
								@enderror
								<!--end::Email-->
							</div>
							<!--end::Input group=-->
							<!--begin::Input group=-->
							<div class="fv-row mb-3">
								<!--begin::Label-->
								<label class="form-label fs-6 text-gray-500">Last Name</label>
								<!--end::Label-->
								<!--begin::Last Name-->
								<input class="form-control bg-transparent" type="text" value="{{ old('last_name') }}" name="last_name"
									autocomplete="off" placeholder="Last Name" />
								<!--end::Input-->
								@error('last_name')
									<label for="last_name" class="text-danger mt-2">{{ $message }}</label>
								@enderror
								<!--end::Email-->
							</div>
							<!--end::Input group=-->
							<!--begin::Input group=-->
							<div class="fv-row mb-3">
								<!--begin::Label-->
								<label class="form-label fs-6 text-gray-500">Email</label>
								<!--end::Label-->
								<!--begin::Email-->
								<input class="form-control bg-transparent" type="text" value="{{ old('email') }}" name="email"
									autocomplete="off" placeholder="Email" />
								<!--end::Input-->
								@error('email')
									<label for="email" class="text-danger mt-2">{{ $message }}</label>
								@enderror
								<!--end::Email-->
							</div>
							<!--end::Input group=-->
							<!--begin::Main wrapper-->
							<div class="fv-row mb-3" data-kt-password-meter="true">
								<!--begin::Label-->
								<label class="form-label text-gray-500 fs-6">Password</label>
								<!--end::Label-->
								<!--begin::Input wrapper-->
								<div class="position-relative mb-3">
									<input class="form-control bg-transparent" type="password" name="password" autocomplete="off"
										placeholder="Password" />
									<!--begin::Visibility toggle-->
									<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
										data-kt-password-meter-control="visibility">
										<i class="bi bi-eye-slash fs-3"><span class="path1"></span><span class="path2"></span><span
												class="path3"></span><span class="path4"></span></i>
										<i class="bi bi-eye d-none fs-3"><span class="path1"></span><span class="path2"></span><span
												class="path3"></span></i>
									</span>
									<!--end::Visibility toggle-->
									<!--begin::Highlight meter-->
									<div class="d-none align-items-center mb-3" data-kt-password-meter-control="highlight">
										<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
										<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
										<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
										<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
									</div>
									<!--end::Highlight meter-->

									<!--end::Input-->
									@error('password')
										<label for="password" class="text-danger mt-2">{{ $message }}</label>
									@enderror
									<!--end::Password-->
								</div>
								<!--end::Input wrapper-->
							</div>
							<!--end::Main wrapper-->
							<!--end::Input group=-->
							<!--begin::Main wrapper-->
							<div class="fv-row mb-3" data-kt-password-meter="true">
								<!--begin::Label-->
								<label class="form-label text-gray-500 fs-6">Confirm Password</label>
								<!--end::Label-->
								<!--begin::Input wrapper-->
								<div class="position-relative mb-3">
									<!--begin::Password-->
									<input class="form-control bg-transparent" type="password" name="password_confirmation" autocomplete="off"
										placeholder="Confirm Password" />
									<!--end::Input-->
									<!--begin::Visibility toggle-->
									<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
										data-kt-password-meter-control="visibility">
										<i class="bi bi-eye-slash fs-3"><span class="path1"></span><span class="path2"></span><span
												class="path3"></span><span class="path4"></span></i>
										<i class="bi bi-eye d-none fs-3"><span class="path1"></span><span class="path2"></span><span
												class="path3"></span></i>
									</span>
									<!--end::Visibility toggle-->
									<!--begin::Highlight meter-->
									<div class="d-none align-items-center mb-3" data-kt-password-meter-control="highlight">
										<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
										<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
										<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
										<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
									</div>
									<!--end::Highlight meter-->

									<!--end::Input-->
									@error('password_confirmation')
										<label for="password_confirmation" class="text-danger mt-2">{{ $message }}</label>
									@enderror
									<!--end::Password-->
								</div>
								<!--end::Input wrapper-->
							</div>
							<!--end::Main wrapper-->

							<div class="d-grid mb-10">
								<button type="submit" id="kt_sign_in_submit" class="btn btn-primary">
									<!--begin::Indicator label-->
									<span class="indicator-label">Sign Up</span>
									<!--end::Indicator label-->
									<!--begin::Indicator progress-->
									<span class="indicator-progress">Please wait...
										<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
									<!--end::Indicator progress-->
								</button>
							</div>
							<!--end::Submit button-->
							<!--begin::Sign up-->
							<div class="text-gray-500 text-center fw-semibold fs-6">Already have an Account?
								<a href="{{ route('login') }}" class="link-primary">Sign in</a>
							</div>
							<!--end::Sign up-->
						</form>
						<!--end::Form-->
					</div>
					<!--end::Content-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Body-->
		</div>
		<!--end::Authentication - Sign-in-->
	</div>
	<!--end::Root-->
@endsection

@section('js')
@endsection
