<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
			$table->id();
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email')->unique();
			$table->timestamp('email_verified_at')->nullable();
			$table->string('password');
            $table->string('phone')->nullable();
            $table->enum('gender', ['male', 'female', 'other'])->nullable();
            $table->text('profile_photo')->nullable();
            $table->text('address')->nullable();
			$table->bigInteger('country_id')->unsigned()->nullable();
			$table->bigInteger('state_id')->unsigned()->nullable();
			$table->bigInteger('city_id')->unsigned()->nullable();
			$table->bigInteger('zipcode')->nullable();
			$table->boolean('status')->default(1);
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
