<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class SuperAdminSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$email = 'admin@admin.com';

		$admin_role = Role::updateOrCreate([
			'name' => 'admin'
		]);

		$user_role = Role::updateOrCreate([
			'name' => 'user'
		]);

		$user = User::updateOrCreate([
			'email' => $email
		], [
			'first_name' => 'Super',
			'last_name'  => 'Admin',
			'password'   => Hash::make(123456)
		]);

		$user = User::where('email', $email)->first();
		$user->markEmailAsVerified();
		$user->assignRole($admin_role);
	}
}
