<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$countries = [
			['name' => 'India', 'code' => 'IN'],
			['name' => 'United States of America', 'code' => 'USA'],
			['name' => 'United Kingdom', 'code' => 'UK'],
			['name' => 'Canada', 'code' => 'CN'],
			['name' => 'New Zealand', 'code' => 'NZ'],
			['name' => 'Australia', 'code' => 'AUS']
		];
		foreach ($countries as $country) {
			Country::updateOrCreate([
				'name' => $country['name'],
				'code' => $country['code']
			]);
		}
	}
}
