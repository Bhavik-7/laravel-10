<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		foreach (Config::get('global.user_roles') as $role) {
			Role::updateOrCreate([
				'name' => $role
			]);
		}
	}
}
