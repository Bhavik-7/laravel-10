<?php

if (!function_exists('json_response')) {
	/**
	 * @param $flag
	 * @param FALSE $message
	 * @param array $data
	 * @return mixed
	 */
	function json_response($flag = FALSE, $message = 'Someting went wrong', $data = []) {
		$response['status']  = $flag;
		$response['message'] = $message;
		$response['data']    = $data;

		return $response;
	}

}

if (!function_exists('uploadFile')) {
	/**
	 * @param $request
	 * @param $folder_name
	 * @param $existing_file_name
	 * @return mixed
	 */
	function uploadFile($request, $folder_name, $existing_file_name = '') {
		$file_name = $existing_file_name;

		if ($request->hasFile('image')) {
			$image           = $request->file('image');
			$file_name       = time().'_'.$image->getClientOriginalName();
			$destinationPath = createPathIfNotExists('images/'.$folder_name);
			$image->move($destinationPath, $file_name);
		}

		return $file_name;
	}

}

if (!function_exists('createPathIfNotExists')) {
	/**
	 * @param $path
	 * @return mixed
	 */
	function createPathIfNotExists($path) {
		$destinationPath = public_path($path);

		if (!file_exists($destinationPath)) {
			mkdir($destinationPath, 0777, true);
		}

		return $destinationPath;
	}

}
