<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail {
	use HasApiTokens, HasFactory, Notifiable, SoftDeletes, HasRoles, Billable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array<int, string>
	 */
	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'password',
		'phone',
		'gender',
		'address',
		'city',
		'state',
		'country',
		'zipcode',
		'status',
		'profile_photo',
		'stripe_id',
		'pm_type',
		'pm_last_four',
		'trial_ends_at'
	];

	/**
	 * The attributes that should be hidden for serialization.
	 *
	 * @var array<int, string>
	 */
	protected $hidden = [
		'password',
		'remember_token'
	];

	/**
	 * The attributes that should be cast.
	 *
	 * @var array<string, string>
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
		'password'          => 'hashed'
	];

	/**
	 * @return mixed
	 */
	public function getNameAttribute() {
		return $this->first_name.' '.$this->last_name;
	}

	/**
	 * @return mixed
	 */
	public function providers() {
		return $this->hasMany(Provider::class, 'user_id', 'id');
	}
}
