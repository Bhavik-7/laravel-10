<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model {
	use HasFactory;
	/**
	 * @var array
	 */
	protected $fillable = [
		'name',
		'code'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function states() {
		return $this->hasMany(State::class);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function cities() {
		return $this->hasMany(City::class);
	}
}
