<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model {
	use HasFactory, SoftDeletes;

	/**
	 * @var array
	 */
	protected $fillable = [
		'provider',
		'provider_id',
		'user_id',
		'avatar'
	];
}
