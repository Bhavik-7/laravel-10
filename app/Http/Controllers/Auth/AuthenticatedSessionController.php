<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller {
	/**
	 * Display the login view.
	 */
	public function create(): View {
		return view('auth.login');
	}

	/**
	 * Handle an incoming authentication request.
	 */
	public function store(LoginRequest $request): RedirectResponse {
		$request->authenticate();

		$request->session()->regenerate();

		if (Auth::user()->hasVerifiedEmail() == FALSE) {
			Auth::logout();

			return redirect()->back()->with('error', 'Please verify your email');
		}

		$redirect = Auth::user()->hasRole('admin') ? route('admin.dashboard') : RouteServiceProvider::HOME;

		return redirect()->intended($redirect);
	}

	/**
	 * Destroy an authenticated session.
	 */
	public function destroy(Request $request): RedirectResponse {
		Auth::guard('web')->logout();

		$request->session()->invalidate();

		$request->session()->regenerateToken();

		return redirect('/');
	}
}
