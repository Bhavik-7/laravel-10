<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class VerifyEmailController extends Controller {
	/**
	 * Mark the authenticated user's email address as verified.
	 */
	public function __invoke(Request $request, User $id): RedirectResponse {
		$user     = $id;
		$redirect = auth()->check() ? RouteServiceProvider::HOME.'?verified=1' : route('login');

		if ($user->hasVerifiedEmail()) {
			return redirect()->intended($redirect)->with('warning', 'Your email is already been verified.');
		}

		$user->markEmailAsVerified();

		return redirect()->intended($redirect)->with('success', 'Your email has been verified.');
	}
}
