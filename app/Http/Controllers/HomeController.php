<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class HomeController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('admin.dashboard');
	}

	public function user_dashboard(): View {
		return view('user.dashboard');
	}

	/**
	 * @param $provider
	 */
	public function socialLogin($provider) {
		return Socialite::driver($provider)->stateless()->redirect();
	}

	/**
	 * @param $provider
	 */
	public function socialLoginCallback($provider) {
		try {
			$social_user = Socialite::driver($provider)->stateless()->setHttpClient(new \GuzzleHttp\Client(['verify' => false])) ->user();
		} catch (ClientException $exception) {
			return response()->json(['error' => 'Invalid credentials provided.'], 422);
		}

		//Generate Random password.
		$chars    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$password = substr(str_shuffle($chars), 0, 10);
		$names    = explode(' ', $social_user->getName());
		$email    = $social_user->getEmail();
		$user     = User::where('email', $email)->first();

		if ($user == NULL) {
			$user = User::create([
				'first_name' => $names[0],
				'last_name'  => $names[1],
				'email'      => $email,
				'password'   => Hash::make($password)
			]);
			$user->assignRole('user');
		}

		$user->providers()->updateOrCreate([
			'provider'    => $provider,
			'provider_id' => $social_user->getId()
		], [
			'avatar' => $social_user->getAvatar()
		]);

		if ($user->hasVerifiedEmail() === FALSE) {
			$user->markEmailAsVerified();
		}

		$user->createOrGetStripeCustomer();
		Auth::loginUsingId($user->id);

		return redirect()->route('dashboard');
	}
}
